var familia={
	'papa':{
		'nombre':'Edgar',
		'apellido':'Contreras',
		'gustos':{
			'musica':['Cumbia', 'Salsas'],
			'Deportes':['Futbol', 'Beisbol']
		}
	},
	'mama':{
		'nombre':'Evelia',
		'apellido':'Santillan',
		'gustos':{
			'musica':['Cumbia','Banda'],
			'comida':['picante','dulce']
		}
	}
};

console.log(familia);